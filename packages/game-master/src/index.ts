import yargs from 'yargs'

const {
  argv,
} = yargs
  .option('port', {
    type: 'number',
    demandOption: true,
    default: 8080,
  })

const {
  port,
} = argv as {
  port: number;
}
