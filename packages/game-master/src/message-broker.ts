import {
  Server as WebSocketServer,
} from 'ws'

let webSocketServer: WebSocketServer | null = null

function startMessageBroker(port: number) {
  if (webSocketServer !== null) {
    throw new Error('Message broker is already added')
  }

  webSocketServer = new WebSocketServer({
    port,
  })

  webSocketServer.on('connection', (webSocket) => {
    webSocket.on('message', (data) => {
    })
  })

  return webSocketServer
}

function connect() {

}

function subscribe(topic: string) {

}

function unsubscribe(topic: string) {

}

function registerHandler(topic: string, handler: () => unknown) {

}

function unregisterHandler(topic: string, handler: () => unknown) {

}

function unregisterAllHandlers(topic: string) {

}

export {
  startMessageBroker,
  connect,
  subscribe,
  unsubscribe,
  registerHandler,
  unregisterHandler,
  unregisterAllHandlers,
}
